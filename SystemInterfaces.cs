﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XEventSystem
{
    /// <summary>
    /// Provides some dev facing information based on the context in which the event was posted.
    /// </summary>
    public interface IEventSourceTracker
    {
        string GetDebugString(SystemEvent e);
    }

    /// <summary>
    /// Responsible for invoking the event handler for all listeners.
    /// </summary>
    public interface IEventPoster
    {
        void FireEvent(SystemEvent e, IEventListenerTracker tracker);
    }

    /// <summary>
    /// Responsible for managing all listeners, and making them available for other EventSystem modules.
    /// </summary>
    public interface IEventListenerTracker
    {
        List<SystemListener> GetListeners(System.Type event_type);
        void AddListener(System.Type event_type, SystemListener listener);
        void RemoveListener(uint listener_id);
    }

    /// <summary>
    /// Creates a listener object that is tracked and used by EventSystem modules to invoke the desired handler
    /// </summary>
    public interface IEventCreator
    {
        SystemListener CreateListener<T>(EventHandler<T> handler, object target = null);
    }

    /// <summary>
    /// Responsible for passing events to the event posting module
    /// </summary>
    public interface IEventProcessor
    {
        void OnEventReceived(SystemEvent e);
        void ProcessEvents(IEventPoster poster, IEventListenerTracker tracker);
    }
}
