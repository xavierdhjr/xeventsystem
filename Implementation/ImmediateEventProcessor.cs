﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XEventSystem
{
    /// <summary>
    /// Calls EventSystem.ProcessEvents() immediately upon receiving an event.
    /// </summary>
    public class ImmediateEventProcessor : IEventProcessor
    {
        SystemEvent _lastEvent;

        public void OnEventReceived(SystemEvent e)
        {
            _lastEvent = e;
            EventSystem.ProcessEvents();
        }

        public void ProcessEvents(IEventPoster poster, IEventListenerTracker tracker)
        {
            poster.FireEvent(_lastEvent, tracker);
        }
    }
}
