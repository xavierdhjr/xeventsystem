﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XEventSystem
{
    /// <summary>
    /// Tracks received events using .NET Dictionaries
    /// </summary>
    public class DefaultEventTracker : IEventListenerTracker
    {
        Dictionary<System.Type, List<SystemListener>> _t_events = new Dictionary<Type, List<SystemListener>>();
        Dictionary<uint, SystemListener> _listeners = new Dictionary<uint, SystemListener>();

        public void AddListener(Type event_type, SystemListener listener)
        {
            bool newly_added = false;
            // Add if not found
            if (!_t_events.ContainsKey(event_type))
            {
                newly_added = true;
                _t_events.Add(event_type, new List<SystemListener>());
            }

            List<SystemListener> listeners = _t_events[event_type];

            if (!newly_added)
            {
                // Check for existing
                for (int i = 0; i < listeners.Count; ++i)
                {
                    if (listeners[i].Id == listener.Id)
                        throw new System.Exception("Listener " + listener.Id + " is already registered as an event listener!");
                }
            }

            _t_events[event_type].Add(listener);
            _listeners.Add(listener.Id, listener);
        }

        public void RemoveListener(uint listener_id)
        {
            if (!_listeners.ContainsKey(listener_id))
                throw new System.Exception("No listener was found with handle: " + listener_id);

            SystemListener listener = _listeners[listener_id];
            List<SystemListener> listeners = GetListeners(listener.ListenerType);

            if (listeners.Count <= 0)
                throw new System.Exception("No listeners were found for type " + listener.ListenerType);

            for (int i = 0; i < listeners.Count; ++i)
            {
                if (listeners[i].Id == listener.Id)
                {
                    listeners.RemoveAt(i);
                    return;
                }
            }

            throw new System.Exception("No listener was found with handle: " + listener.Id);
        }

        public List<SystemListener> GetListeners(Type event_type)
        {
            List<SystemListener> listeners = new List<SystemListener>();
            _t_events.TryGetValue(event_type, out listeners);

            return listeners;
        }


    }

}
