﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XEventSystem
{
    /// <summary>
    /// Creates a regular ol' SystemListener, assigning a unique identifier to it.
    /// </summary>
    public class DefaultEventCreator : IEventCreator
    {
        static uint next_listener_id = 0;

        public SystemListener CreateListener<T>(EventHandler<T> handler, object target = null)
        {
            System.Type t_type = typeof(T);

            SystemListener l = new SystemListener()
            {
                Handler = handler,
                Id = next_listener_id++,
                Obj = target,
                ListenerType = typeof(T)
            };

            return l;
        }
    }
}
