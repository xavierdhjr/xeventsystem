﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XEventSystem
{
    /// <summary>
    /// Enqueues the specified event to fire at a later time, with the option to limit the amount of dequeued events.
    /// </summary>
    public class QueuedEventProcessor : IEventProcessor
    {
        int _eventsToProcess;
        Queue<SystemEvent> _postedEventQueue = new Queue<SystemEvent>();

        public QueuedEventProcessor()
        {
            _eventsToProcess = 100;
        }

        public QueuedEventProcessor(int eventsPerProcess)
        {
            _eventsToProcess = eventsPerProcess;
        }

        public void OnEventReceived(SystemEvent e)
        {
            _postedEventQueue.Enqueue(e);
        }

        public void ProcessEvents(IEventPoster poster, IEventListenerTracker tracker)
        {
            while (true)
            {
                for (int i = 0; i < _eventsToProcess && i < _postedEventQueue.Count; ++i)
                {
                    poster.FireEvent(_postedEventQueue.Dequeue(), tracker);
                }
            }
        }
    }

}
