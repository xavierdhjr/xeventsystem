﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace XEventSystem
{
    public class StackTraceSourceTracker : IEventSourceTracker
    {
        public string GetDebugString(SystemEvent e)
        {
#if DEBUG
            StackTrace trace = new StackTrace(true);
            return trace.ToString();
#else
            return "";
#endif
        }
    }
}
