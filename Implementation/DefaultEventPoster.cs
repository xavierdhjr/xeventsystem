﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Reflection;

namespace XEventSystem
{
    /// <summary>
    /// Uses reflection to invoke event handlers for the given event type on all listeners, with the option to filter by target object.
    /// </summary>
    public class DefaultEventPoster : IEventPoster
    {
        public void FireEvent(SystemEvent e, IEventListenerTracker tracker)
        {
            object event_obj = e.Payload;
            System.Type t_type = e.EventType;

            List<SystemListener> listeners = tracker.GetListeners(t_type);

            if (listeners == null)
                return;

            // Fire this event for all relevant listeners
            for (int i = 0; i < listeners.Count; ++i)
            {
                SystemListener l = listeners[i];

                // If the listener object is equal to the target, or the listener does not have a target
                if (e.Target == null || e.Target == l.Obj)
                {
                    System.Delegate d = (System.Delegate)l.Handler;
                    l.PostCount++;
                    try
                    {
                        d.Method.Invoke(d.Target, new object[] { event_obj });
                    }
                    catch (System.Exception ex)
                    {
                        PreserveStackTrace(ex.InnerException);
                        throw ex.InnerException;
                    }
                }

            }
        }

        private static void PreserveStackTrace(Exception exception)
        {
            MethodInfo preserveStackTrace = typeof(Exception).GetMethod("InternalPreserveStackTrace",
              BindingFlags.Instance | BindingFlags.NonPublic);
            preserveStackTrace.Invoke(exception, null);
        }
    }
}
