## Purpose ##

This system provides a framework for firing and receiving strongly typed events with minimal overhead. 

## Usage ##

Any class with a default constructor may be used as an event.

* Creating an event class and using it
```csharp
public class MyEvent
{
	public string MyString;
	public int MyInt;
}

...

EventListener _listener;

public MyListenerClass()
{
	EventSystem.Init() // Iniitalize the event system once, anywhere. 
	_listener = new EventListener();
	_listener.Listen<MyEvent>(OnMyEvent);
}

...

void OnMyEvent(MyEvent e)
{
	// Handle this event
}
```

* Posting an event
```csharp
	EventSystem.Post(new MyEvent(){ MyString = "Beans", MyInt = 10 });
```

## Extending / Modifying the System ##

Key parts of the system are broken up into easily replaceable modules. 

### Interfaces ###

* *IEventPoster*
	* Responsible for invoking the event handler for all listeners.
	* The default implementation uses reflection to invoke event handlers for the given event type on all listeners, with the option to filter by target object.
* *IEventListenerTracker*
	* Responsible for managing all listeners, and making them available for other EventSystem modules.
	* The default implementation tracks listeners using .NET dictionaries.
* *IEventCreator*
	* Creates a listener object that is tracked and used by EventSystem modules to invoke the desired handler.
	* The default implementation creates a regular ol' SystemListener, and assigns a unique identifier to it. The next UID is incremented for every new event passed to the processor.
* *IEventProcessor*
	* Responsible for passing events to the event posting module.
	* The default implementation immediately passes events to their handlers as soon as the event is posted.
	* A second implementation is also provided, which queues events and fires them in order when ProcessEvents() is called on the EventSystem.
```csharp
	void Update()
	{
		// Process all queued events every frame.
		EventSystem.ProcessEvents();
	}
```
* *IEventSourceTracker*
	* Provides some dev facing information based on the context in which the event was posted.
	* The default implementation adds a stack trace of the event to every event if DEBUG is set.

### Replacing Modules ###

All modules are created on system Init(). To replace the modules, simply provide your own modules object and swap out any functionality you want.

```csharp
    EventSystem.Modules modules = new EventSystem.Modules()
    {
    	SystemEventTracker = new MyCustomEventTracker()
    };

    EventSystem.Init(modules);
```

# License #

Copyright (c) 2017 Xavier Durand-Hollis Jr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

