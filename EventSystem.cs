﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace XEventSystem
{
    public class SystemListener
    {
        public System.Type ListenerType;
        public object Handler;
        public uint Id;
        public object Obj;
        public bool Destroy;
        public int PostCount;
    }

    public class SystemEvent
    {
        public System.Type EventType;
        public object Payload;
        public object Target;
        public object Source;
    }

    public delegate void EventHandler(object[] args);
    public delegate void EventHandler<T>(T arg);

    public class EventSystem
    {
        public class Modules
        {
            public IEventListenerTracker SystemEventTracker = new DefaultEventTracker();
            public IEventPoster SystemEventPoster = new DefaultEventPoster();
            public IEventCreator SystemEventCreator = new DefaultEventCreator();
            public IEventProcessor SystemEventProcessor = new ImmediateEventProcessor();
            public IEventSourceTracker SystemEventSourceTracker = new StackTraceSourceTracker();
        }

        public static void Init()
        {
            Init(new Modules());
        }

        public static void Init(Modules event_modules)
        {
            if (_system != null)
                _system = null;

            _system = new EventSystem();

            _systemEventCreator = event_modules.SystemEventCreator;
            _systemEventPoster = event_modules.SystemEventPoster;
            _systemEventProcessor = event_modules.SystemEventProcessor;
            _systemEventTracker = event_modules.SystemEventTracker;
            _systemEventSourceTracker = event_modules.SystemEventSourceTracker;
        }

        /// <summary>
        /// Posts this event. If any listeners are listening for an object of this type, they will
        /// fire.
        /// </summary>
        public static void Post(object e)
        {
            _system.post(e, null);
        }

        /// <summary>
        /// Posts this event to a specific target. That target will fire its handler if it is listening.
        /// </summary>
        public static void Post(object e, object target)
        {
            _system.post(e, target);
        }

        /// <summary>
        /// Any time event T is posted, handler will fire.
        /// </summary>
        public static uint Listen<T>(EventHandler<T> handler)
        {
            return _system.listen<T>(handler, null);
        }

        /// <summary>
        /// Only events that are posted directly to this object will fire /handler/
        /// </summary>
        public static uint Listen<T>(EventHandler<T> handler, object targetted_listener)
        {
            return _system.listen<T>(handler, targetted_listener);
        }

        public static void StopListen(uint handle)
        {
            _system.stop_listen(handle);
        }

        static EventSystem _system;

        static IEventListenerTracker _systemEventTracker;
        static IEventPoster _systemEventPoster;
        static IEventCreator _systemEventCreator;
        static IEventProcessor _systemEventProcessor;
        static IEventSourceTracker _systemEventSourceTracker;

        static uint next_listener_id = 0;

        public static void ProcessEvents()
        {
            _systemEventProcessor.ProcessEvents(_systemEventPoster, _systemEventTracker);
        }

        private EventSystem()
        {
            if (_system != null)
            {
                throw new System.InvalidOperationException("An event system has already been created.");
            }
        }

        void post(object event_obj, object target)
        {
            System.Type event_type = event_obj.GetType();
            SystemEvent created_event = new SystemEvent()
            {
                EventType = event_type,
                Payload = event_obj,
                Target = target
            };

            // Get debug data
            created_event.Source = _systemEventSourceTracker.GetDebugString(created_event); 

            // Process this event
            _systemEventProcessor.OnEventReceived(created_event);
        }

        uint listen<T>(EventHandler<T> handler, object listener)
        {
            SystemListener created_listener = _systemEventCreator.CreateListener<T>(handler, listener);
            _systemEventTracker.AddListener(typeof(T), created_listener);

            return created_listener.Id;
        }

        void stop_listen(uint listener_handle)
        {
            _systemEventTracker.RemoveListener(listener_handle);
        }


        void fire(SystemEvent e)
        {
            _systemEventPoster.FireEvent(e, _systemEventTracker);
        }

    }

}
