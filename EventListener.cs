﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XEventSystem
{
    /// <summary>
    /// Helper class that holds on to listener handles and can easily clean them up.
    /// </summary>
    public class EventListener
    {
        List<uint> handles = new List<uint>();

        /// <summary>
        /// Listens to an event of type T with an optional target object.
        /// </summary>
        /// <typeparam name="T">Event type to post</typeparam>
        /// <param name="handler">Delegate that handles this event.</param>
        /// <param name="target">Events that are posted to this target object will be received by the listener</param>
        public void Listen<T>(EventHandler<T> handler, object target = null)
        {
            handles.Add(EventSystem.Listen<T>(handler, target));
        }

        /// <summary>
        /// Removes all handles managed by this listener from the EventSystem.
        /// </summary>
        public void StopListenAll()
        {
            foreach (uint u in handles)
                EventSystem.StopListen(u);

            handles = new List<uint>();
        }
    }
}
